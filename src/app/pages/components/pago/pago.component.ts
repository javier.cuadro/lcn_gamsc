import { Component, OnInit } from '@angular/core';
import { resultadoLineamiento, actualizarEstadoPagoLineamiento } from '../../../interfaces/lcn.interfaces';

import { LcnService } from '../../../services/lcn.service';
import { ActualizarEstado } from '../../../classes/actualizarEstado';
import { ClsActualizarEstadoPago } from '../../../classes/clsActualizarEstadopago';
import { URLReporte } from '../../../classes/urlReporte';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {


  actualizarEstado:ActualizarEstado


  resultadolineamiento:resultadoLineamiento;
  idLineamiento :number;
  ActualizarEstadoPagoLineamiento:ClsActualizarEstadoPago;
  urlReporte:any;
  



  constructor(private lcnService:LcnService) { }

  ngOnInit(): void {            
    this.urlReporte=new URLReporte(0,'','');
  }

  Pagar(){

    this.actualizarEstado= new ActualizarEstado(0,'',0);
    this.ActualizarEstadoPagoLineamiento= new ClsActualizarEstadoPago(0);

    //localStorage.setItem('resultadoLineamiento', JSON.stringify(resp));

    if(localStorage.getItem('resultadoLineamiento')){
      this.actualizarEstado =JSON.parse(localStorage.getItem('resultadoLineamiento')!);
      console.log("actualizarEstado.idlineamiento: "+this.actualizarEstado.idlineamiento);
      
      //this.idLineamiento=this.resultadolineamiento.idlineamiento;
      
      //this.ActualizarEstadoPagoLineamiento.idlineamiento=this.actualizarEstado.idlineamiento; 
      
      //this.ActualizarEstadoPagoLineamiento.idlineamiento=this.actualizarEstado.idlineamiento;
      //this.lcnService.ActualizaEstadoLineamiento("{'idlineamiento':" +this.idLineamiento +"}");
      //this.lcnService.ActualizaEstadoLineamiento( this.ActualizarEstadoPagoLineamiento);

    }

    this.lcnService.ActualizaEstadoLineamiento(
      {
        "idlineamiento": this.actualizarEstado.idlineamiento        
      }     
    ).subscribe((result)=>{
      console.warn(result);
    });

    
    //ObtenerReportePdfLineamiento
    //cadena.substr(inicio[, longitud])
    let cadena,cadena1:string;

    //data:image/png;base64,
    cadena=JSON.parse(localStorage.getItem('imagenLcn'));
    
    cadena1=cadena.substring(22, cadena.length-1);
    console.log("Cadena: "+cadena1);

    this.lcnService.ObtenerReportePdfLineamiento({
        "idlineamiento": this.actualizarEstado.idlineamiento,
        "imagen64": cadena1}).subscribe((resp)=>{
      this.urlReporte=resp;
      console.log(resp);
      //this.urlReporte=resp;
      console.log("RutaReporte"+this.urlReporte.urldocumento);
      this.goToLink(this.urlReporte.urldocumento);
    });

    
    

    
    alert("El pago ha sido realizado");

  }
  goToLink(url: string){ window.open(url, "_blank"); }
  

}
