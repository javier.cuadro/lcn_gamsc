import { Component,Renderer2,ViewChild,ElementRef} from '@angular/core';
import * as htmlToImage from 'html-to-image';

//import { icon, latLng, marker, polyline, tileLayer, StreetLabels geoJSON, StreetLabels) } from 'leaflet';
import { geoJSON, GeoJSONOptions,icon,control ,latLng, Layer, layerGroup, Map, marker, point, polyline, tileLayer } from 'leaflet';

//import {StreetLabels} from '@leaflet.streetlabels'


import { MapasService } from 'src/app/services/mapas.service';
import { Marcador } from 'src/app/classes/marcador.class';


import * as L from 'leaflet';
//import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
//import * as M from 'leaflet-streetlabels';
//import * as SM from 'leaflet-search';
//import * as LeafletSearch from 'leaflet-search';

import "leaflet-search/dist/leaflet-search.min.css";
import "leaflet-search/dist/leaflet-search.min.js";

//import "leaflet-search";


////import * as SL from 'leaflet-streetlabels';
//import { M } from 'leaflet-streetlabels';

import { CallesService } from 'src/app/services/calles.service';
//import { Calle } from '../../classes/calle';
//import { Calle } from '../../classes/calle.class';
import { Calle } from 'src/app/classes/calle.class';




//import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';
import { async } from '@angular/core/testing';


//declare let L;


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent {

  //@ViewChild('idMap') private IDMAP: ElementRef;
  

  ContadorMapa:number=0;  

   public geo_json_data;
    title = 'Sig alcaldia';
    json;
    layerFarmacias;
    map;
    streetLabelsRenderer;
    marcadores : Marcador[]=[];
    marker;
    i=0;
    
  
    lat = -17.7876515;
    lng = -63.1825049;
    lat1=0;
    lng1=0;
    calle= new Calle(0,'','','','');
    //cliente = new Cliente(0,'','','','','');
    //marcadores : Marcador[]=[];
    
  
    /*public layersControl = {
      baseLayers: { }
    };*/
    //public map: new L.Map('map');
     //map = L.map('map');
    //.setView([51.505, -0.09], 13);
  
    //fieldsVias = ["id", "nombre", "jerarquia", "categoria", "anchovia"];
     
  
    
    constructor( private mapaService:MapasService,private calleService:CallesService,private router:Router,private renderer:Renderer2) { 
      //console.log('ss');
    }
    
    geoJSon;//:GeoJSON.GeoJSON;
    
    
  
  
    // Define our base layers so we can reference them multiple times
    streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19,
    });
  
    esri = L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 19,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    });
  
    wMaps = L.tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
      attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 19,
      //ext: 'png'
      }
    );

    wmsLayers = L.tileLayer.wms("http://localhost:8080/geoserver/nyc/wms?",{
        layers: "ejes_vias",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );
    wmsLayers_uv = L.tileLayer.wms("http://localhost:8080/geoserver/nyc/wms?",{
        layers: "uv",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );
   
  
    
  
    // Layers control object with our two base layers and the three overlay layers
    /*layersControl = {
      baseLayers: {
        'Street Maps': this.streetMaps,
        'Wikimedia Maps': this.wMaps
      },
      overlays: {
        'Mt. Rainier Summit': this.summit,
        'Mt. Rainier Paradise Start': this.paradise,
        'Mt. Rainier Climb Route': this.route
      }
    };*/
  
  
  
    options = {
      layers: [ this.streetMaps,this.esri,this.wMaps, this.wmsLayers],
      zoom: 13,
      center: L.latLng([ -17.7876515, -63.1825049 ])
    
      //lat = -17.7876515;
      //lng = -63.1825049;
    };
  
    
    /*geoJSON = {
      id: 'geoJSON',
      name: 'Geo JSON Polygon',
      enabled: true,
      layer: L.geoJSON(this.geo_json_data)
    };*/
     getColor(d) {
      return d > 100000000 ? '#800026' :
      d > 50000000 ? '#BD0026' :
      d > 4 ? '#EFEFEF' :
      d > 3 ? '#000287' :
      d > 2 ? '#ccff00' :
      d > 1 ? '#d9880f' :
      d > 0 ? '#0b5e05' :
      '#FFEDA0'}
      
      
  
    style = feature => {
      return {
        weight: 3,
        opacity: 1,
        //color: 'blue',
        dashArray: '1',
        fillOpacity: 0.7,
        color: this.getColor(feature.properties.tiposvia)
      };
    }

     popup(feature, layer) {
      if (feature.properties && feature.properties.nombrevia) {
        layer.bindPopup(feature.properties.nombrevia);
      }
  
      /*for(let i in layer.features)
      layer.features[i].properties.color = this.getColor( layer.features[i].properties.jerarquia );*/
    }
    
  
  
    /*resetHighlight = e => {
      geojson.resetStyle(e.target);
      info.update();
    }*/
  
    //zoomToFeature = e => {
  //    this.map.fitBounds(e.target.getBounds());
  //  }
  
  
    /*onEachFeature = (feature, layer) => {
      layer.on({
        //mouseover: this.highlightFeature,
        //mouseout: this.resetFeature,
        //click: this.zoomToFeature
        html = "",
        for (prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);         
      });
    }*/
  
     onEachFeature(feature, layer) {
      //if (feature.properties && feature.properties.popupContent) {
          //layer.bindPopup(feature.properties.popupContent);
          for(let i in layer.features){
            layer.features[i].properties.color = this.getColor( layer.features[i].properties.tiposvia );
            //console.log(layer.features[i].properties.jerarquia);
          }
  
          /*let html = "";
        for (let prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);         */
      //}
      }
  
       
  
  
  
    onMapReady(map) {

      
    //this.IDMAP.nativeElement.remove();

     



      //alert('aaa');
      L.Icon.Default.imagePath = "assets/leaflet/";
      //L.Icon.Default.imagePath=
      let geojson;
      //let info =  L.control;
      this.map = map;
      //console.log(SL);
      //console.log('ss');
    
      //////////////////////////////
      if(localStorage.getItem('marcadores')){
        this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
        //alert('Entro');
        
      }

      for(let marcador of this.marcadores){          
        this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');
        //this.marker.push(LamMarker);
        //LamMarker.addTo(this.map);   
        //this.map.fitBounds(this.marker);    
        this.map.addLayer(this.marker);
        //map.fitBounds( latlng.layer.getBounds() );
        //var zoom = map.getBoundsZoom(this.marker.layer.getBounds());
        //map.setView(this.marker.latlng, 12); // access the zoom
  
        this.i=1;
      }
      ////////////////////////////////////////
  
  
      
  
  
      /*info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
      };
  
      info.update = function (props) {
        this._div.innerHTML = '<h4>US Population Density</h4>' + (props ?
          '<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
          : 'Hover over a state');
      };
  
      info.addTo(map);*/
  
  
      /*L.control.zoom({
        position: 'bottomright'
      }).addTo(map);*/
  
  
      /*let legend = L.control.({position: 'bottomright'});
      legend.onAdd = function (map) {
       let div = L.DomUtil.create('div', 'info legend');
      
      div.innerHTML +=
      '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="120" />';    
      
      
      };
      legend.addTo(this.map);*/
  
       
  
      const legend = new (L.Control.extend({
        options: { position: 'bottomright' }
      }));
  
  
      legend.onAdd = function (map) {
        const div = L.DomUtil.create('div', 'Leyenda');
        
        div.innerHTML = '<div><b>Leyenda</b></div>';
        
          div.innerHTML += '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="80" />';    
          //div.innerHTML += '<img alt="legend" src="../.. " width="127" height="80" />';    
        
          
        
        return div;
      };
      legend.addTo(this.map);
  
  
    
      var baseMaps = {
        
        "Detallado": this.wMaps,
        "Predeterminado": this.streetMaps,
        "UV": this.wmsLayers_uv,
        //"Lineamientos":this.wmsLayers,
        "Satélite": this.esri
    };
   
    
      this.mapaService.obtenerMapas().subscribe((data)=>{
        this.geo_json_data = data;
        //console.log(this.geo_json_data);
        //this.initStatesLayer();
  
      /*  geojson = L.geoJSON(this.geo_json_data, {
          style: this.style,
          //onEachFeature: this.onEachFeature,
          onEachFeature: this.popup,        
          
        }).addTo(this.map);*/
      
  
        /*var overlayMaps = {
          "Lineamientos": geojson
        };*/

        var overlayMaps = {
          "Lineamientos": this.wmsLayers
        };
          //this.esri,this.wMaps
        L.control.layers(baseMaps,overlayMaps,{
          position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
          collapsed: false // true
        }).addTo(this.map);

        //const map = L.map('map').setView([51.505, -0.09], 13);

        
        
  
        
  
        /*let searchControl = new (L.Control.Search({
          layer: geojson,
          initial:false,
          propertyName: 'nombre',
          circleLocation: true,
          moveToLocation: function (latlng) {
              map.setView(latlng, 17);
              }
          }));
    
          searchControl.on('search:locationfound', function(e) {
              e.layer.openPopup();
          });
          map.addControl(searchControl);*/
  
  
  
  
  
  
  
  
        L.control.scale().addTo(this.map);



        var markersLayer = new L.LayerGroup();	//layer contain searched elements
      
        map.addLayer(markersLayer);
      //markersLayer.addLayer(L.geoJSON(this.geo_json_data),);
      //map.addLayer(markersLayer);
      


 let searchText=new L.control.search({
        position:'topleft',		
        layer: markersLayer,
        initial: false,
        //propertyName: 'UV',
        zoom: 12,
        circleLocation: false,
        marker: false,
        textPlaceholder: 'Búsqueda de UV',
        /*moveToLocation: function(latlng, title, map) {
          //map.fitBounds( latlng.layer.getBounds() );
          var zoom = map.getBoundsZoom(latlng.layer.getBounds());
            map.setView(latlng, zoom); // access the zoom
        }*/
      });            
      

      searchText.on('search:locationfound', function(e) {		
            //console.log('search:locationfound', );
        
            //map.removeLayer(this._markerSearch)
            alert('search:locationfound');
          
            e.layer.setStyle({fillColor: '#3f0', color: '#0f0'});
            if(e.layer._popup)
              e.layer.openPopup();
        
          }).on('search:collapsed', function(e) {
           
        
            /*this..eachLayer(function(layer) {	//restore feature color
              this.geoJSon.resetStyle(layer);
            });	*/
      });
      map.addControl( searchText );



      let data1 = [
        {"loc":[41.575330,13.102411], "title":"aquamarine"},
        {"loc":[41.575730,13.002411], "title":"black"},
        {"loc":[41.807149,13.162994], "title":"blue"},
        {"loc":[41.507149,13.172994], "title":"chocolate"},
        {"loc":[41.847149,14.132994], "title":"coral"},
        {"loc":[41.219190,13.062145], "title":"cyan"},
        {"loc":[41.344190,13.242145], "title":"darkblue"},	
        {"loc":[41.679190,13.122145], "title":"Darkred"},
        {"loc":[41.329190,13.192145], "title":"Darkgray"},
        {"loc":[41.379290,13.122545], "title":"dodgerblue"},
        {"loc":[41.409190,13.362145], "title":"gray"},
        {"loc":[41.794008,12.583884], "title":"green"},	
        {"loc":[41.805008,12.982884], "title":"greenyellow"},
        {"loc":[41.536175,13.273590], "title":"red"},
        {"loc":[41.516175,13.373590], "title":"rosybrown"},
        {"loc":[41.506175,13.273590], "title":"royalblue"},
        {"loc":[41.836175,13.673590], "title":"salmon"},
        {"loc":[41.796175,13.570590], "title":"seagreen"},
        {"loc":[41.436175,13.573590], "title":"seashell"},
        {"loc":[41.336175,13.973590], "title":"silver"},
        {"loc":[41.236175,13.273590], "title":"skyblue"},
        {"loc":[41.546175,13.473590], "title":"yellow"},
        {"loc":[41.239190,13.032145], "title":"white"}
      ];
    
      //var map = new L.Map('map', {zoom: 9, center: new L.latLng(data[0].loc) });	//set center from first location
    
      
      
    
      var controlSearch = new L.control.search({
        position:'topright',		
        layer: markersLayer,
        initial: false,
        zoom: 12,
        marker: false
      });
    
      map.addControl( controlSearch );
      //console.log(L.geoJSON(this.geo_json_data),);

      
      //markersLayer.addLayer(overlayMaps);
    
      ////////////populate map with markers from sample data
      for(let i in data1) {
        let title = data1[i].title,	//value searched
           loc = data1[i].loc,		//position found
          marker = new L.Marker( L.latLng(41.436175,13.573590), {title: title} );//se property searched
        marker.bindPopup('title: '+ title );
        markersLayer.addLayer(marker);
      }
        //L.control.search().addTo(this.map);


      /*  var controlSearch = new L.Control.Search({
          position:'topright',		
          layer: markersLayer,
          initial: false,
          zoom: 12,
          marker: false
        });
      
        map.addControl( controlSearch );
        */

        /*(L.Control as any).search({
          position:'topleft',		
          layer: this.geoJSon,
          initial: false,
          zoom: 12,
          marker: false
        }).addTo(this.map);*/

        //L.control.minimap(osm2, { toggleDisplay: true }).addTo(map);

        //legend.addTo(this.map);


        //var searchLayer = L.geoJSON().addTo(map);
        //... adding data in searchLayer ...
        //L.map('map', { leafletSearch: {layer: searchLayer} });


       /* var searchControl = new L.Control.Search({
          layer: this.geoJSon,
          propertyName: 'name',
          marker: false,
         
        });*/
         //L.control.search().setLayer(this.geoJSon);




         // Add a Search bar to the map
   /*var searchControl = new L.control.search({
		layer: this.geoJSon,
		propertyName: 'lad16nm',
		marker: false,
		moveToLocation: function(latlng, title, map) {
			var zoom = map.getBoundsZoom(latlng.layer.getBounds()) - 1;
           	map.setView(latlng, zoom); // access the zoom
		}
	});
	
    searchControl.on('search:locationfound', function(e) {
		e.layer.setStyle({weight: "2", opacity: "1"});
	});
	
	map.addControl( searchControl );  //inizialize search control
  */
       
        
        
        

        /*var controlSearch =  L.control.search({
          layer: new L.LayerGroup()
      }).on('search_expanded', function () {
          console.log('search_expanded!')
      }).addTo(map);/
        




      

        /*searchControl.on('search:locationfound', function(e) {
		
          //console.log('search:locationfound', );
      
          //map.removeLayer(this._markerSearch)
      
          e.layer.setStyle({fillColor: '#3f0', color: '#0f0'});
          if(e.layer._popup)
            e.layer.openPopup();
      
        }).on('search:collapsed', function(e) {
      
          this.geoJSon.eachLayer(function(layer) {	//restore feature color
            this.geoJSon.resetStyle(layer);
          });	
        });
        
        map.addControl( searchControl );  //inizialize search control*/

        //minimap(osm2, { toggleDisplay: true }).addTo(map);
  
  
        //////////////
        //M.StreetLabels.default({
        /*this.streetLabelsRenderer =  SL.StreetLabels({
          collisionFlg : true,
          propertyName : 'nombre',
          showLabelIf: function(layer) {
            return true; //layer.properties.type == "primary";
          },
          fontStyle: {
            dynamicFontSize: false,
            fontSize: 10,
            fontSizeUnit: "px",
            lineWidth: 4.0,
            fillStyle: "black",
            strokeStyle: "white",
          },
        })*/
  
        // Create a new map and attach the renderer created above:
        /* map = new M.Map('map', {
          renderer : this.streetLabelsRenderer, //Custom Canvas Renderer
        });*/
  
      });


      /*var controlSearch = new L.Control.Search({
        position:'topright',		
        layer: markersLayer,
        initial: false,
        zoom: 12,
        marker: false
      });*/

     
      //let searchText=new L.control.search();   
      
      /*L.control.search({
        position:'topleft',		
        layer: this.geoJSon,
        initial: false,
        propertyName: 'nombrevia',
        zoom: 12,
        circleLocation: false,
        marker: false,
        textPlaceholder: 'Búsqueda de UV',
        moveToLocation: function(latlng, title, map) {
          //map.fitBounds( latlng.layer.getBounds() );
          var zoom = map.getBoundsZoom(latlng.layer.getBounds());
            map.setView(latlng, zoom); // access the zoom
        }
      }).addTo(this.map);*/

      //var markersLayer = new L.LayerGroup();
//map.addLayer(markersLayer);

/*var controlSearch = new L.control.search({
  position:'topleft',
  propertyName: 'nombrevia',
  textPlaceholder: 'Búsqueda de UV',
  layer: this.geoJSon,
  initial: false,
  zoom: 12,
  marker: false
});

map.addControl( controlSearch );*/

      //console.log(markersLayer);
      


      //var searchLayer = L.layerGroup().addTo(map);
//... adding data in searchLayer ...
     


    //map.addControl(L.control.search({        
      //  layer: this.geoJSon,
        
        //propertyName: 'nombrevia',                
      //}) );


     /* L.control.search({
        layer: this.geoJSon,
        initial: false,
        jsonpParam: 'json_callback',
        propertyName: 'nombrevia',
        buildTip: function(text, val) {
          var type = val.layer.feature.properties.nombrevia;
          return '<a href="#" class="'+type+'">'+text+'<b>'+type+'</b></a>';
        }
      })
      .addTo(map);*/

      
      
      //(L.control as any).search({
        //layer: this.geoJSon,
        //initial: false,
        //jsonpParam: 'json_callback',
        //propertyName: 'nombrevia',
        //buildTip: function(text, val) {
//          var type = val.layer.feature.properties.nombrevia;
          //return '<a href="#" class="'+type+'">'+text+'<b>'+type+'</b></a>';
        //}
      //}).addTo(this.map);
  
      
  
    }
  
    
    /*
     initStatesLayer() {
      const stateLayer = L.geoJSON(this.geo_json_data, {
        style: (feature) => ({
          weight: 3,
          opacity: 0.5,
          color: '#008f68',
          fillOpacity: 0.8,
          fillColor: '#6DB65B'
        }),
        onEachFeature: (feature, layer) => (
          layer.on({
            mouseover: (e) => (this.highlightFeature(e)),
            mouseout: (e) => (this.resetFeature(e)),
          })
        )
      });
    console.log(this.geo_json_data);
      //this.map.addLayer(stateLayer);
    }
    
     
    
    
    highlightFeature(e: L.LeafletMouseEvent) {
      const layer = e.target;
    
      layer.setStyle({
        weight: 10,
        opacity: 1.0,
        color: '#DFA612',
        fillOpacity: 1.0,
        fillColor: '#FAE042'
      });
    }
    
    resetFeature(e: L.LeafletMouseEvent) {
      const layer = e.target;
    
      layer.setStyle({
        weight: 3,
        opacity: 0.5,
        color: '#008f68',
        fillOpacity: 0.8,
        fillColor: '#6DB65B'
      });
    }
  */
    
  
  
   
  
  
  
  
  
  agregarMarcadorCalle(evt){
    
     //this.map=evt;
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng
    
  
    this.GeogToUTM(latitud,longitud);

    this.imagen(latitud , longitud);

    this.calleService.obtenerDonde(this.lat1,this.lng1).subscribe(calle =>{ this.calle = calle[0]
    
      console.log(calle); 
      if (calle.length>0){                     
        
        
        this.marcadores.pop();//  =[];//.splice(0,this.marcadores.length);      
        if (this.i>0){
          this.map.removeLayer(this.marker);
          this.borrarMarcador(this.i );
        }        
        let nuevoMarcador = new Marcador(latitud , longitud);
        nuevoMarcador.nombrevia=calle[0].nombrevia;
        nuevoMarcador.ancho_via=calle[0].ancho_via;
        nuevoMarcador.grupolineamiento=calle[0].idgrupolin;

        
        

        this.marcadores.push(nuevoMarcador);
        this.guardarStorage();
        for(let marcador of this.marcadores){          
          this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');
          //this.marker.push(LamMarker);
          //LamMarker.addTo(this.map);       
          this.map.addLayer(this.marker);
    
          this.i=1;
        }
      }else{
        alert('Acerquesé mas a su acera');
      }

    });
    
  
  
      /*var punto = L.marker([this.lat1, this.lng1]).bindPopup('Soy un puntazo');
      punto.addTo();*/
        
    }

    
  
  
  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;
  
    let lng0 :number;
    
  
  
  
    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
      Item = 1;//Default
  
  
  
    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters. 
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    
    
    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian
  
    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    //alert(a+"   "+b);
    //alert(1-(b/a)*(b/a));
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    //alert(e);
    //Input Geographic Coordinates
    //Decimal Degree Option
    
    //latd0 = parseFloat(document.getElementById("DDLatBox0").value);
    //lngd0 = parseFloat(document.getElementById("DDLonBox0").value);
    
    //latd1 = Math.abs(parseFloat(document.getElementById("DLatBox0").value));
    //latd1 = latd1 + parseFloat(document.getElementById("MLatBox0").value)/60;
    //latd1 = latd1 + parseFloat(document.getElementById("SLatBox0").value)/3600;    
  
    lngd=lngd0;
    latd=latd0;
    
    
    
    xd = lngd;
    yd = latd;
  //        alert(Item);
  //        alert(usft);
  
    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}
      
    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    //alert(a*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256))));
    //alert(a*(esq*(3/8 + esq*(3/32 + 45*esq/1024))));
    //alert(a*(esq*esq*(15/256 + esq*45/1024)));
    //alert(a*esq*esq*esq*(35/3072));
    //alert(M);
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    //alert("M    ="+M);
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard 
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;
  
    //document.getElementById("UTMeBox1").value = Math.round(10*(x))/10 / usft;
    //document.getElementById("UTMnBox1").value = Math.round(10*y)/10 /usft;
    //if (phi<0){document.getElementById("SHemBox").checked=true;}
    
    //ajaxmagic();			
  }//close Geog to UTM
  
  /*refresh(map){
    //alert('entro');
    
  }*/

  
  guardarStorage(){
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores) );
  }

  borrarMarcador(i : number){
    //console.log(i);
    this.marcadores.splice(i,1);
    this.guardarStorage();
    //this.snackBar.open('Marcador borrado', 'Cerrar',{duration : 3000});
  }
  


  toDataURL = async (url) => {
    console.log("Downloading image...");
    var res = await fetch(url);
    var blob = await res.blob();

    const result = await new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })

    return result
  };

 

   CargarMap1(lat:number,lng:number) {
    const width = 200;
    const height = 200;
    this.ContadorMapa++;
    let mapElement = document.createElement("div");
    
    //mapElement.setAttribute("id", "div");
    //mapElement.setAttribute("class", "map");
    mapElement.style.width = `${width}px`;
    mapElement.style.height = `${height}px`;    
    document.body.appendChild(mapElement);

    let map = L.map(mapElement, {
      attributionControl: false,
      zoomControl: false,
      fadeAnimation: false,
      zoomAnimation: false
    }).setView([lat, lng], 18);
    //etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
    const tileLayer = L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 19,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    }).addTo(map);
    /*const tileLayer = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    );//.addTo(map);
    map.addLayer(tileLayer);  */
    //map.          

    L.marker([lat, lng]).addTo(map);

    L.circle([lat, lng], {
      color: "red",
      fillColor: "#f03",
      fillOpacity: 0.1,
      radius: 50
    }).addTo(map);



    let ele:any;
    let image64:any;
    
    //this.resolveAfter2Seconds(image64,mapElement);

    //this.getValueWithAsync(mapElement);

    this.getValueWithPromise(mapElement); 

    //mapElement.style.width = `30px`;
    //mapElement.style.height = `30px`;    
    //console.log(image64);

    
    //mapElement.style.

    //ele=document.querySelector("#map");    
    //mapElement.querySelectorAll;

    //const all = document.getElementById('div'+this.ContadorMapa)
    //console.log(ele);
  //map.g
    /*mapElement.getMap().then(function(map) {
      setTimeout(function() {
          map.invalidateSize();
          map._resetView(map.getCenter(), map.getZoom(), true);
      }, 200);
  });*/
    
    
    
    //document.querySelectorAll("div");    
    
       

     /* htmlToImage.toPng(document.getElementById('idMap'+this.ContadorMapa))
      .then(function (dataUrl) {
        //download(dataUrl, 'my-node.png');
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
        console.log(dataUrl); // this prints only data:,
        });

        htmlToImage.toPng(mapElement)
      .then(function (dataUrl) {
        //download(dataUrl, 'my-node.png');
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
        console.log(dataUrl); // this prints only data:,
        });*/
        

     //new Promise(resolve => tileLayer.on("load", () => resolve()));
    //const dataURL = await htmlToImage.toPng(mapElement, { width, height });
    //document.body.removeChild(mapElement);

   
   
    
    
    //mapElement=L.map(mapElement)

    //
    //document.getElementById("idMap")
    


    

    
  }

  resolveAfter2Seconds(image64,mapElement) {
    return new Promise(resolve => {
      setTimeout(() => {
        //resolve(x);

        htmlToImage.toPng(mapElement      , { quality: 0.95 })
      .then((dataUrl) => {        
         //console.log(dataUrl); 
         image64=dataUrl;// this prints only data:,
         //console.log("#idMap"+this.ContadorMapa); // this prints only data:,                 
         //console.log("Imagen64: "+dataUrl); 

         localStorage.setItem('imagenLcn', JSON.stringify(dataUrl) );

         
         mapElement.hidden=true;

         /*var img = new Image();
          img.src = dataUrl;
          document.body.appendChild(img);*/
          return image64;
      });
      }, 2000);
    });
  }

  async getValueWithAsync(mapElement) {
    const value = <any>await this.resolveAfter2Seconds(20,mapElement);
    console.log(`async result: ${value}`);
  }

  getValueWithPromise(mapElement) {
    this.resolveAfter2Seconds(2,mapElement).then(value => {
      console.log(`promise result: ${value}`);

      
    });
    console.log('I will not wait until promise is resolved');
  }

   generarImagen(){
    let ele:any;
    //let variable="#idMap"+this.ContadorMapa;
    let variable="div"+ this.ContadorMapa;
    console.log(variable);
    ele=document.querySelectorAll(variable);
    console.log(ele);
    //map.once
    //map.
  
     //htmlToImage.toPng(document.getElementById("idMap"+this.ContadorMapa)  , { quality: 0.95 })
     htmlToImage.toPng(ele  , { quality: 0.95 })
     
      .then((dataUrl) => {
        //var img = new Image();
        //img.src = dataUrl;
        //document.body.appendChild(document.getElementById("idMap"));
        //download(dataUrl, 'my-node.png');
         //console.log(dataUrl); // this prints only data:,
         //localStorage.removeItem("imagenPlano");
         //localStorage.
         localStorage.removeItem('imagenPlano');
          localStorage.setItem('imagenPlano', JSON.stringify(dataUrl) );
          this.recuperar();
        //alert(dataUrl);

        /*const imgElement = document.createElement("img");
        imgElement.src = dataUrl;
        document.body.appendChild(imgElement);*/
    });
  }

  
  irSiguiente(){

    //this.router.navigate(['catastro']);    
   //this.generateImage();
    
    this.router.navigate(['pages/construccion']);
  }
  imagen(lat:number,lng:number){

      //console.log(lat);
    this.CargarMap1(lat,lng);
    //this.generarImagen();
  
    //this.IDMAP.nativeElement.remove();
    //this
    //mapElement.hidden=true;
    //this.renderer.setAttribute("idMap", "hidden", "true");
    //this.startTimer();
    //var capa=document.getElementById("idMap");
    //capa.style.display="none";
    //capa.style.visibility="hidden";

  }

  startTimer() {
    let timeLeft: number = 3;
    let interval;
    interval = setInterval(() => {
      if(timeLeft > 0) {
        timeLeft--;
      } else {
        timeLeft = 60;
      }
    },1000)
  }
  recuperar(){
    console.log(JSON.parse(localStorage.getItem('imagenPlano')));

  }


}
