import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { LcnService } from '../../../services/lcn.service';
import { Categorias } from 'src/app/interfaces/lcn.interfaces';
import { Subcategorias } from 'src/app/interfaces/lcn.interfaces';
import { Actividades } from 'src/app/interfaces/lcn.interfaces';
import { switchMap,tap } from 'rxjs';
import { Marcador } from 'src/app/classes/marcador.class';

import {Router} from '@angular/router';
import { TipoEdificacion } from '../../../classes/tipoedificacion';



@Component({
  selector: 'app-construccion',
  templateUrl: './construccion.component.html',
  styles: [
  ]
})
export class ConstruccionComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({

    categoria:['',Validators.required],
    subcategoria:['',Validators.required],
    actividad:['',Validators.required],
    nombre: [ , [ Validators.required, Validators.minLength(3) ]   ],
    precio: [ , [ Validators.required, Validators.min(0)] ],
    existencias: [ , [ Validators.required, Validators.min(0)] ],
  })


  categorias    :  Categorias[]=[];
  subcategorias :  Subcategorias[]=[];
  actividades   :  Actividades[]=[];

  cargando: boolean= false;

  marcadores : Marcador[]=[];
  tipoEdificacion: TipoEdificacion;

  constructor( private fb: FormBuilder, private lcnService:LcnService, private router:Router ) { }

  ngOnInit() {
    this.tipoEdificacion= new TipoEdificacion(0,0,0);


    if(localStorage.getItem('marcadores')){
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
      //alert('Entro');
      console.log(this.marcadores[0].grupolineamiento);
    }

    


    this.miFormulario.reset({
      categoria:['',Validators.required],      
      subcategoria:['',Validators.required],      

    })

    this.lcnService.getGrupoCategorias(this.marcadores[0].grupolineamiento).subscribe(grupoCategorias=>{
      //alert('Entro');
      //console.log("grupolineamiento: "+this.marcadores[0].grupolineamiento);  
      console.log(grupoCategorias);
      this.categorias=grupoCategorias;
      /*this.categorias.unshift({
        nombrecategoria: '[ Seleccione Pais ]',
        idcategoria:0
      })*/
    });     

    

//////////////////////////////////////////////////////////
    this.miFormulario.get('categoria')?.valueChanges
    .pipe(
      ///esto es para resetear
      tap(( _ )=>{
        this.miFormulario.get('subcategoria')?.reset('');
        //this.miFormulario.get('frontera')?.disable();
        this.cargando=true;

      }),
      switchMap(categoria=> this.lcnService.getSubcategorias(categoria))
    )
    .subscribe(subcategorias=>{
      //console.log(subcategorias);
      this.subcategorias  = subcategorias;
      this.cargando=false;

    });
    //////////////////////////////////////////////////////

    this.miFormulario.get('subcategoria')?.valueChanges
    .pipe(
      ///esto es para resetear
      tap(( _ )=>{
        this.miFormulario.get('actividad')?.reset('');
        //this.miFormulario.get('frontera')?.disable();
        //this.cargando=true;

      }),
      switchMap(subcategoria=> this.lcnService.getActividadesPorCodigo(subcategoria))
    )
    .subscribe(actividades=>{
      //console.log(actividades);
      this.actividades  = actividades;
      this.cargando=false;

    });
    



  }

    //this.paisesService.getPaisesPorRegion(region))
    //this.categorias= this.categorias.
  


  campoEsValido( campo: string ) {

    return this.miFormulario.controls[campo].errors 
            && this.miFormulario.controls[campo].touched;
  }

  guardar() {

    if ( this.miFormulario.invalid )  {
      this.miFormulario.markAllAsTouched();
      return;
    }


    console.log(this.miFormulario.value);
    this.miFormulario.reset();
  }


  irAnterior(){
    //window.history.back();
    this.router.navigate(['pages/mapa']);

  }

  irSiguiente(){

    //this.router.navigate(['catastro']);

    //console.log(parseInt(this.miFormulario.get('categoria')?.value));


    this.tipoEdificacion.idcategoria    = parseInt(this.miFormulario.get('categoria')?.value);
    this.tipoEdificacion.idsubcategoria = parseInt(this.miFormulario.get('subcategoria')?.value);
    this.tipoEdificacion.idactividad    = parseInt(this.miFormulario.get('actividad')?.value);    
    localStorage.setItem('tipoEdificacion', JSON.stringify(this.tipoEdificacion));
    
    
    this.router.navigate(['pages/catastro']);
  }

}
