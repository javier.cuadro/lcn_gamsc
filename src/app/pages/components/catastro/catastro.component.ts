import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl  } from '@angular/forms';


import { LcnService } from '../../../services/lcn.service';
import { Categorias } from 'src/app/interfaces/lcn.interfaces';
import { Subcategorias } from 'src/app/interfaces/lcn.interfaces';
import { Actividades } from 'src/app/interfaces/lcn.interfaces';
import { Clasifi_Activi_Regla }  from 'src/app/interfaces/lcn.interfaces';

import { switchMap,tap } from 'rxjs';
import { Marcador } from 'src/app/classes/marcador.class';

import {Router} from '@angular/router';
import { TipoEdificacion } from '../../../classes/tipoedificacion';
import { PlanoSuelo } from '../../../classes/planosuelo';
import { LCN } from '../../../classes/lcn';


@Component({
  selector: 'app-catastro',
  templateUrl: './catastro.component.html',
  styles: [
  ]
})
export class CatastroComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({

    categoria:['',Validators.required],
    subcategoria:['',Validators.required],
    actividad:['',Validators.required],
    nombre: [ , [ Validators.required, Validators.minLength(3) ]   ],
    precio: [ , [ Validators.required, Validators.min(0)] ],
    existencias: [ , [ Validators.required, Validators.min(0)] ],
    
    numerplano: [ '1', Validators.required ],
	  superficietitulo: [ '', Validators.required ],
	  superficiemensura: [ '', Validators.required ],
	  zona: [ '', Validators.required ],
	  manzana: [ '', Validators.required ],
	  distrito: [ '', Validators.required ],
	  lote: [ '', Validators.required ],
	  tipousosuelo: [ '', Validators.required ],
	  ubicacionlote: [ '', Validators.required ],
	  tipovia: [ '', Validators.required ],
	  idpersona:[ '', Validators.required ],    
    anchoVia1:[ '', Validators.required ]
  })


  categorias    :  Categorias[]=[];
  subcategorias :  Subcategorias[]=[];
  actividades   :  Actividades[]=[];
  Clasifi_Activi_Regla :Clasifi_Activi_Regla;//[]=[];


  respuestaPlanoSuelo:any;
  respuestaLineamiento:any;

  cargando: boolean= false;

  marcadores : Marcador[]=[];
  anchoVia:string;
  planoSuelo:PlanoSuelo;
  lineamiento:LCN;
  idplanoSuelo:number;
  idtipoLineamiento:number;
  idActividad:number;
  idgrupoLineamiento:number;
  idClasifi_Activi_Regla:number;

  tipoEdificacion:TipoEdificacion;

  constructor( private fb: FormBuilder, private lcnService:LcnService,private router:Router ) { }

  ngOnInit() {
    this.tipoEdificacion= new TipoEdificacion(0,0,0);
    this.planoSuelo= new PlanoSuelo('','','','','','','','','','',0,"",0);
    this.lineamiento= new LCN("","","","",1,1,1,1,1,1,0,0);



    if(localStorage.getItem('marcadores')){
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
      this.anchoVia=this.marcadores[0].ancho_via.toString();
      this.idgrupoLineamiento= this.marcadores[0].grupolineamiento


      //this.miFormulario.controls['anchoVia1'].setValue(this.marcadores[0].ancho_via.toString());
      //this.miFormulario .patchValue(this.unrecurso);
      //this.miFormulario.controls['anchoVia1'].patchValue(this.marcadores[0].ancho_via.toString());
      this.miFormulario.get("anchoVia1").setValue(this.marcadores[0].ancho_via.toString());
      //this.datos.get('totalIngresos').setValue(valor);

      //this.survey.controls['account'].patchValue(survey.account);
      //this.survey.controls['account'].setValue(survey.account);

      //this.anchoVia1="11";
      //this.miFormulario.setValue(this.anchoVia1,"anchoVia")
      //this.miFormulario.setVa

      console.log(this.marcadores[0].ancho_via.toString());
      
      //this.miFormulario.anchoVia.value=2;

      //alert('Entro');
      //console.log(this.marcadores[0].grupolineamiento);
    }

    if(localStorage.getItem('tipoEdificacion')){
      this.tipoEdificacion = JSON.parse(localStorage.getItem('tipoEdificacion'));            
      this.idActividad=this.tipoEdificacion.idactividad;

      console.log(this.tipoEdificacion);  
    }


    this.miFormulario.reset({
      categoria:['',Validators.required],      
      subcategoria:['',Validators.required],      

    })

    this.lcnService.getGrupoCategorias(this.marcadores[0].grupolineamiento).subscribe(grupoCategorias=>{
      //console.log("grupolineamiento: "+this.marcadores[0].grupolineamiento);  
      //console.log(grupoCategorias);
      this.categorias=grupoCategorias;
      /*this.categorias.unshift({
        nombrecategoria: '[ Seleccione Pais ]',
        idcategoria:0
      })*/
    });     

//////////////////////////////////////////////////////////
    this.miFormulario.get('categoria')?.valueChanges
    .pipe(
      ///esto es para resetear
      tap(( _ )=>{
        this.miFormulario.get('subcategoria')?.reset('');
        //this.miFormulario.get('frontera')?.disable();
        this.cargando=true;

      }),
      switchMap(categoria=> this.lcnService.getSubcategorias(categoria))
    )
    .subscribe(subcategorias=>{
      console.log(subcategorias);
      this.subcategorias  = subcategorias;
      this.cargando=false;

    });
    //////////////////////////////////////////////////////

    this.miFormulario.get('subcategoria')?.valueChanges
    .pipe(
      ///esto es para resetear
      tap(( _ )=>{
        this.miFormulario.get('actividad')?.reset('');
        //this.miFormulario.get('frontera')?.disable();
        //this.cargando=true;

      }),
      switchMap(subcategoria=> this.lcnService.getActividadesPorCodigo(subcategoria))
    )
    .subscribe(actividades=>{
      console.log(actividades);
      this.actividades  = actividades;
      this.cargando=false;

    });
    



  }

    //this.paisesService.getPaisesPorRegion(region))
    //this.categorias= this.categorias.
  


  campoEsValido( campo: string ) {

    return this.miFormulario.controls[campo].errors 
            && this.miFormulario.controls[campo].touched;
  }

  guardar() {

    if ( this.miFormulario.invalid )  {
      this.miFormulario.markAllAsTouched();
      return;
    }


    //console.log(this.miFormulario.value);
    this.miFormulario.reset();
  }

  irAnterior(){
    //window.history.back();
    this.router.navigate(['pages/construccion']);

  }


  registroUsoSuelo(val:any)
    {
       
      this.lcnService.registrarUsoSuelo(
       {
         "numerplano": "59",
         "superficietitulo": "100",
         "superficiemensura": "101",
         "zona": "F1",
         "manzana": "120",
         "distrito": "9",
         "lote": "8",
         "tipousosuelo": "NORMAL",
         "ubicacionlote": "ESQUINA",
         "tipovia": "1",
         "idpersona": 1
       }     
     ).subscribe((result)=>{
       console.warn(result);
     })  
    }  

  irSiguiente(){
//console( this.registroUsoSuelo)
//this.registroUsoSuelo(val);

    
    /*numerplano: string;
    superficietitulo: string;
    superficiemensura: string;
    zona: string;
    manzana: string;

    
    distrito: string;
    lote: string;
    tipousosuelo: string;
    ubicacionlote: string;
    tipovia: string;
    idpersona:number;
    */    
    this.planoSuelo.numerplano='2';
    this.planoSuelo.superficietitulo=this.miFormulario.get('superficietitulo')?.value;
    this.planoSuelo.superficiemensura=this.miFormulario.get('superficiemensura')?.value;
    this.planoSuelo.zona=this.miFormulario.get('zona')?.value;
    this.planoSuelo.manzana=this.miFormulario.get('manzana')?.value;
    this.planoSuelo.distrito=this.miFormulario.get('distrito')?.value;
    this.planoSuelo.lote=this.miFormulario.get('lote')?.value;            
    this.planoSuelo.tipousosuelo=this.miFormulario.get('tipousosuelo')?.value;
    this.idtipoLineamiento= parseInt( this.miFormulario.get('ubicacionlote')?.value);
    this.planoSuelo.uv="0";
    this.planoSuelo.estadopago =0;

    console.log(this.miFormulario.get('ubicacionlote')?.value);
    switch (this.miFormulario.get('ubicacionlote')?.value){
      case "1":
        this.planoSuelo.ubicacionlote="INTERMEDIO";
        break;
      case "2":
        this.planoSuelo.ubicacionlote="ESQUINA";
        break;

      case "3":
        this.planoSuelo.ubicacionlote="CALLE A CALLE";
        break;
      default:
         console.log("No such day exists!");
        break;
    }

    //this.planoSuelo.ubicacionlote=this.miFormulario.get('ubicacionlote')?.value

    this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;        

    if(Math.floor(Math.random() * (10 + 1)) >5){
      this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;        
    }else{
      this.planoSuelo.tipovia="NORMAL"

    }

    this.planoSuelo.idpersona=1;//this.miFormulario.get('idpersona')?.value     
           

    /*this.lcnService.createPlanoSuelo(
      {
        "numerplano": "59",
        "superficietitulo": "100",
        "superficiemensura": "101",
        "zona": "F1",
        "manzana": "120",
        "distrito": "9",
        "lote": "8",
        "tipousosuelo": "NORMAL",
        "ubicacionlote": "ESQUINA",
        "tipovia": "1",
        "idpersona": 1
      }     
    ).subscribe((result)=>{
      console.warn(result);
    })  */

    

    //this.lcnService.getGrupoCategorias   (this.marcadores[0].grupolineamiento)                               .subscribe(grupoCategorias=>{
    
      //alert('Entro');
      //console.log("grupolineamiento: "+this.marcadores[0].grupolineamiento);  
    //  console.log(clasifi_activi_regla);
      //this.idClasifi_Activi_Regla  =clasifi_activi_regla[0].id_clasi_activi_regla  ;
      /*this.categorias.unshift({
        nombrecategoria: '[ Seleccione Pais ]',
        idcategoria:0
      })*/
    //});    
    
   

    //console.log("id_clasi_activi_regla: "+this.Clasifi_Activi_Regla.id_clasi_activi_regla); 
      //console.log("grupolineamiento: "+this.marcadores[0].grupolineamiento);  

      //console.log(this.idActividad+'-'+this.idgrupoLineamiento+'-'+this.idtipoLineamiento)
      
      //this.Clasifi_Activi_Regla.id_clasi_activi_regla =clasifi_activi_regla ;
      //this.Clasifi_Activi_Regla=clasifi_activi_regla;
    //  console.log("clasifi_activi_regla = "+ this.Clasifi_Activi_Regla);
      //this.Clasifi_Activi_Regla[0].id_clasi_activi_regla;
      
      /*this.categorias.unshift({
        nombrecategoria: '[ Seleccione Pais ]',
        idcategoria:0
      })*/
    //});     

    

     this.lcnService.createPlanoSuelo(this.planoSuelo).subscribe(resp=>{
      this.respuestaPlanoSuelo=resp;
      console.warn(resp);
      this.idplanoSuelo=parseInt(this.respuestaPlanoSuelo.idplanosuelo);
      console.warn(this.idplanoSuelo);

        /*
        public numerotramite: string,
        public oficiodale: string,  
        public fechatramite: string, //01/04/2022"
        public fotolote: string,
        public idgrupolineamiento:number,
        public idactividad:number,
        public idpersona:number,
        public idtipolineamiento:number,
        public idplanosuelo:number,
        public idexcepcion:number,
        public id_clasi_activi_regla:number,
        public estadopago:number*/
        console.log(this.idplanoSuelo); 
      this.lineamiento.numerotramite="1";
      this.lineamiento.oficiodale="002/2021";
      this.lineamiento.fechatramite="2022-01-04 00:00:00";
      this.lineamiento.fotolote="";
      this.lineamiento.idgrupolineamiento=this.idgrupoLineamiento;
      this.lineamiento.idactividad=this.idActividad,
      this.lineamiento.idpersona=1;
      this.lineamiento.idtipolineamiento=this.idtipoLineamiento;
      this.lineamiento.idplanosuelo=this.idplanoSuelo;
      this.lineamiento.idexcepcion=3;
      this.lineamiento.id_clasi_activi_regla=this.idClasifi_Activi_Regla;
      this.lineamiento.estadopago=0;    

      this.lcnService.registrarLineamiento(this.lineamiento).subscribe(resp=>{   
        
        /*this.respuestaPlanoSuelo=resp;
        console.warn(resp);
        this.idplanoSuelo=parseInt(this.respuestaPlanoSuelo.idplanosuelo);*/
        console.warn(resp);
        //localStorage.setItem('idlineamiento', resp);
        localStorage.setItem('resultadoLineamiento', JSON.stringify(resp));

      //console.warn(this.idplanoSuelo);      

      });

        


        
      });

  
      

     // console.log('respuesta de Plano Suelo--- '+this.respuestaPlanoSuelo);

     



    alert('LINEAMIENTO GENERADO CORRECTAMENTE, SE PROCEDE A REALIZAR EL PAGO');
   // this.router.navigate(['pages/pago'])
   
   this.router.navigate(['pages/pago']);;;
  }


  buscarRegla(){


    console.log("ubicacion:" + this.miFormulario.get('ubicacionlote')?.value);
    switch (this.miFormulario.get('ubicacionlote')?.value){
      case "1":
        this.idtipoLineamiento=1
        break;
      case "2":
        this.idtipoLineamiento=2
        break;

      case "3":
        this.idtipoLineamiento=3
        break;
      default:
        this.idtipoLineamiento=1
         console.log("No such day exists!");
        break;
    }

    this.lcnService.getClasifi_Activi_Regla(this.idActividad,this.idgrupoLineamiento  ,this.idtipoLineamiento );
    this.Clasifi_Activi_Regla =JSON.parse(localStorage.getItem('clasifi_activi_regla')!);
    this.idClasifi_Activi_Regla=this.Clasifi_Activi_Regla.id_clasi_activi_regla;

    console.log("Primer id_clasi_activi_regla: "+this.idClasifi_Activi_Regla); 
  }


}
