import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapaComponent } from './components/mapa/mapa.component';
import { CatastroComponent } from './components/catastro/catastro.component';
import { ConstruccionComponent } from './components/construccion/construccion.component';
import { PagoComponent } from './components/pago/pago.component';

const routes: Routes = [
  {
  path:'',
  children:[
    {path:'mapa',component:MapaComponent},
    {path:'catastro',component:CatastroComponent},
    {path:'construccion',component:ConstruccionComponent},
    {path:'pago',component:PagoComponent},
    
    {path:'**',redirectTo:'mapa'}
  ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
