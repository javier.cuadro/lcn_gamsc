import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { PagesRoutingModule } from './pages-routing.module';
import { MapaComponent } from './components/mapa/mapa.component';
import { CatastroComponent } from './components/catastro/catastro.component';
import { ConstruccionComponent } from './components/construccion/construccion.component';
import { PagoComponent } from './components/pago/pago.component';





@NgModule({
  declarations: [
    MapaComponent,
    CatastroComponent,
    ConstruccionComponent,
    PagoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PagesRoutingModule,
    LeafletModule,
    FormsModule
  ]
})
export class PagesModule { }
