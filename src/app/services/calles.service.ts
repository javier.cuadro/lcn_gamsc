import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CallesService {

  url = 'http://localhost/apirestphp_pg/calles';

  constructor(private http: HttpClient) { }

  
  obtenerDonde(lat: number,lng:number): Observable<any> {
    return this.http.get(`${this.url}/seleccionar.php?lat=`+lat +`&lng=`+lng);
  }
}
