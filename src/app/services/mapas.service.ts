import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MapasService {
  url = 'http://localhost/apirestphp_pg/mapas';
  constructor(private http: HttpClient) { }

  obtenerMapas():Observable<any>{
    return this.http.get(`${this.url}/listar.php`);
  }
}
