import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, of, combineLatest, Subject } from 'rxjs';

import { GrupoCategoria } from '../interfaces/lcn.interfaces';
import { Categorias } from '../interfaces/lcn.interfaces';
import { Subcategorias } from '../interfaces/lcn.interfaces';
import { Actividades } from '../interfaces/lcn.interfaces';
import { Clasifi_Activi_Regla } from '../interfaces/lcn.interfaces';


import { map } from 'rxjs/operators'
import { LCN } from '../classes/lcn';
import { PlanoSuelo } from '../classes/planosuelo';
import { URLReporte } from '../classes/urlReporte';




@Injectable({
  providedIn: 'root'
})
export class LcnService {

  constructor(private http: HttpClient) { }

  //http://207.244.229.255:5007/api/lcnCategoriaLineamiento/gruposcategorias?idgrupo=1
  private baseUrl:string ='http://207.244.229.255:5006/api/';
  private baseUrl1:string='http://207.244.229.255:5007/api/';
  public clasifi_activi_regla :Clasifi_Activi_Regla;

  //private _regiones: string[]=['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];

  getGrupoCategorias( grupoLineamiento: Number){

    const url: string =`${this.baseUrl1}/lcnCategoriaLineamiento/gruposcategorias?idgrupo=${ grupoLineamiento}`;
    console.log(url);

    return this.http.get<GrupoCategoria[]>(url)
    .pipe(
      map( ( resp:GrupoCategoria[] ) => 
         //resp.map( pais => ({ nombre:pais.name, codigo:pais.alpha3Code })
         resp.map( grupoCategoria => ({ nombre:grupoCategoria.nombrecategoria, idcategoria:grupoCategoria.idcategoria }))
      )      
    );

  }

  getSubcategorias ( codigo: string):Observable<Subcategorias[]>{    

    if (!codigo){
      return of([])
    }
    const url: string =`${this.baseUrl}/subcategorias?idcat=${ codigo}`;
    return this.http.get<Subcategorias[]>(url);
  }

  getActividadesPorCodigo ( codigo: string):Observable<Actividades[]>{
    if (!codigo){
      return of([])
    }
    const url: string =`${this.baseUrl}/actividades?idsub=${codigo}`;
    return this.http.get<Actividades[]>(url);
  }


  createLCN(lcn: LCN): Observable<any>{
    //http://207.244.229.255:5007/api/lcnLineamiento/Registro
    const url: string =`${this.baseUrl1}/lcnLineamiento/Registro`;
    return this.http.post(url, lcn);
  }





  createPlanoSuelo(planoSuelo: any){
    //http://207.244.229.255:5007/api/lcnLineamiento/Registro
    const url: string =`${this.baseUrl1}/lcnPlanoSuelo/Registro`;    
    return this.http.post(url, planoSuelo);
  }       

   registrarUsoSuelo(data:any){
    console.log("registrar uso de suelo ev 200");
    let url="http://207.244.229.255:5007/api/lcnPlanoSuelo/Registro";
    //let url="/api/Proveedor/Registrar";
    return this.http.post(url,data);
  }


  //http://207.244.229.255:5007/api/lcnReglaLineamiento/ObtenerRegla/gruposcategorias?idactividad=5&idgrupolineamiento=5&idtipolineamiento=1

  
  /*getClasifi_Activi_Regla( idActividad: number,idgrupoLineamiento:number,idtipoLineamiento:number):Observable<Clasifi_Activi_Regla>{    
  
      const url: string =`${this.baseUrl1}/lcnReglaLineamiento/ObtenerRegla/gruposcategorias?idactividad=${ idActividad }&idgrupolineamiento=${ idgrupoLineamiento }&idtipolineamiento=${ idtipoLineamiento }`;
      return this.http.get<Clasifi_Activi_Regla>(url);
   }*/
   getClasifi_Activi_Regla( idActividad: number,idgrupoLineamiento:number,idtipoLineamiento:number):any
   {
     //console.log("entro");
  
    const url: string =`${this.baseUrl1}lcnReglaLineamiento/ObtenerRegla/gruposcategorias?idactividad=${ idActividad }&idgrupolineamiento=${ idgrupoLineamiento }&idtipolineamiento=${ idtipoLineamiento }`;
    console.log(url);
    return this.http.get<Clasifi_Activi_Regla>(url).subscribe(resp=>{
      console.log("Services clasifi_activi_regla:" + resp.id_clasi_activi_regla);

      //this.clasifi_activi_regla = resp;
      localStorage.setItem('clasifi_activi_regla',JSON.stringify(resp));

      //return data;
    });


    /*return this.http.get<GrupoCategoria[]>(url)
    .pipe(
      map( ( resp:GrupoCategoria[] ) => 
         //resp.map( pais => ({ nombre:pais.name, codigo:pais.alpha3Code })
         resp.map( grupoCategoria => ({ nombre:grupoCategoria.nombrecategoria, idcategoria:grupoCategoria.idcategoria }))
      )      
    );*/
   }

  //    );

    //console.log(url);    
    /*return this.http.get<Clasifi_Activi_Regla[]>(url)
    .pipe(
      map(( resp:Clasifi_Activi_Regla[] ) => 
         //resp.map( pais => ({ nombre:pais.name, codigo:pais.alpha3Code })
           resp.map( clasifi_activi_regla => ({ id_clasi_activi_regla:clasifi_activi_regla.id_clasi_activi_regla,
                                                idactividad:clasifi_activi_regla.idactividad ,
                                                idgrupolineamiento:clasifi_activi_regla.idgrupolineamiento,
                                                idpdf:clasifi_activi_regla.idpdf,                                                
                                                idregla:clasifi_activi_regla.idregla,
                                                idtipolineamiento:clasifi_activi_regla.idtipolineamiento 
                                              }))
          )      
    );
  }*/

  



  registrarLineamiento(data:any){
    console.log("registrar uso de suelo ev 200");
    let url="http://207.244.229.255:5007/api/lcnLineamiento/Registro";
    //let url="/api/Proveedor/Registrar";
    return this.http.post(url,data);
  }

  ActualizaEstadoLineamiento(idLineamiento: any){
    //http://207.244.229.255:5007/api/lcnLineamiento/Registro
    const url: string =`${this.baseUrl1}lcnLineamiento/ActualizarEstado`;    
    console.log(idLineamiento);
    console.log(url);
    let texto:string
    //texto='{"idlineamiento":'+idLineamiento +'}';
    
    //"numerplano": "59",
    return this.http.post(url,idLineamiento);
  }    

  ObtenerReportePDF<URLReporte>(idLineamiento){
  //http://207.244.229.255:5007/api/lcnReporteLineamiento/ObtenerReporte?idlineamiento=56

  const url: string =`${this.baseUrl1}lcnReporteLineamiento/ObtenerReporte?idlineamiento=${ idLineamiento }`;
    console.log(url);
    return this.http.get<URLReporte>(url);
    /*subscribe(resp=>{
      console.log("rutaReporte:" + resp);    
      localStorage.setItem('rutaReporte',JSON.stringify(resp));

      
    });*/

  }


  ObtenerReportePdfLineamiento(data:any){
    //http://207.244.229.255:5007/api/lcnReporteLineamiento/ObtenerReporte?idlineamiento=56
  
    const url: string =`${this.baseUrl1}lcnReporteLineamiento/ObtenerReporteLineamiento`;
    //http://207.244.229.255:5007/api/  lcnReporteLineamiento/ObtenerReporteLineamiento
      console.log(url);
      console.log(data);
      //return this.http.get<URLReporte>(url);

      return this.http.post(url,data);
      /*subscribe(resp=>{
        console.log("rutaReporte:" + resp);    
        localStorage.setItem('rutaReporte',JSON.stringify(resp));
  
        
      });*/
  
    }
 }



  

   /*getExchangeRate = async (fromCurrency, toCurrency) => {
    const response = await axios.get('http://data.fixer.io/api/latest?    access_key=[yourAccessKey]&format=1');
    const rate= response.data.rates;
    const euro = 1 / rate[fromCurrency];
    const exchangeRate = euro * rate[toCurrency];
    
    return getExchangeRate;
  
  }*/





  

