export interface Categorias {
    idcategoria: number;
    nombre:      string;
}

export interface Subcategorias {
    idsubcategoria: number;
    nombre:         string;
    idcategoria:    number;
}

export interface Actividades {
    idactividad:    number;
    nombre:         string;
    idsubcategoria: number;
}


export interface GrupoCategoria {
    idgrupolineamiento: number;
    idcategoria:        number;
    nombrecategoria:    string;
}

export interface Clasifi_Activi_Regla {
    id_clasi_activi_regla: number;
    idactividad:           number;
    idgrupolineamiento:    number;
    idpdf:                 number;
    idregla:               number;
    idtipolineamiento:     number;
}


export interface resultadoLineamiento {
    codigo:        number;
    mensaje:       string;
    idlineamiento: number;
}
export interface actualizarEstadoPagoLineamiento {
    idlineamiento: number;
}