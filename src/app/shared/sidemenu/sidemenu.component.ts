import { Component } from '@angular/core';

interface MenuItem{
 texto:string;
 ruta:string; 
}

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styles: [
  ]
})
export class SidemenuComponent  {

  pagesMenu: MenuItem[]=[
    {
      texto:'1.- Ubicación Domicilio',
      ruta: './pages/mapa'
    },
    {
      texto:'2.- Datos Construccción',
      ruta: './pages/construccion'
    },
    {
      texto:'3.- Datos Catastro',
      ruta: './pages/catastro'
    },
    {
      texto:'4.- Pago de la solicitud',
      ruta: './pages/pago'
    }

  ];

  

}
