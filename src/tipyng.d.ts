import * as L from 'leaflet';
declare module 'leaflet' {
  namespace control {
    //function fullscreen(options?: any): any;
    function minimap(layer : any, options?: any):control.MiniMap;
    function search(options?: any):void ;            
    function search(options?: any):control.Search ;            
    //function OSMGeocoder():Control.OSMGeocoder;    
    //function _onMainMapMoved(e : any) : Control.MiniMap;      }
  namespace control {
    interface MiniMap {
      addTo(map: L.Map): any;
      //_onMainMapMoved(e:any);
    }

    interface Search {
      addTo(map: L.Map): any;
      
      
      //_onMainMapMoved(e:any);
    }
    //interface OSMGeocoder {}
  }
}
}